package com.zadatak.service;

import com.zadatak.domain.AuthUser;

/**
 * Created by marko on 02.03.17..
 */
public interface AuthUserService {

    AuthUser findByLoginName(String loginName);

    AuthUser findOne(Long userId);

}

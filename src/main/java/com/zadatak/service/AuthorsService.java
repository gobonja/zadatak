package com.zadatak.service;

import com.zadatak.domain.Authors;

import java.util.List;

/**
 * Created by marko on 12.03.17.
 */
public interface AuthorsService {

    List<Authors> findAll();

    Authors findOne(Long authorId);

    Authors save(Authors authors);
}

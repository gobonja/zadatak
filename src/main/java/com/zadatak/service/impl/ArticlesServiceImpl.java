package com.zadatak.service.impl;

import com.zadatak.domain.Articles;
import com.zadatak.repository.ArticlesRepository;
import com.zadatak.service.ArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by marko on 12.03.17.
 */
@Service
public class ArticlesServiceImpl implements ArticlesService {

    @Autowired
    private ArticlesRepository articlesRepository;

    @Override
    public List<Articles> findAll() {
        return articlesRepository.findAll();
    }

    @Override
    public List<Articles> findByAuthUserLoginName(String loginName) {
        return articlesRepository.findByAuthUserLoginName(loginName);
    }

    @Override
    public Articles save(Articles articles) {
        return articlesRepository.save(articles);
    }

    @Override
    public Articles findOne(Long articleId) {
        return articlesRepository.findOne(articleId);
    }

    @Override
    public void delete(Articles articles) {
        articlesRepository.delete(articles);
    }

    @Override
    public void deleteById(Long id) {
        articlesRepository.delete(id);
    }


}

package com.zadatak.service.impl;

import com.zadatak.domain.AuthUser;
import com.zadatak.domain.UserPrincipal;
import com.zadatak.service.AuthUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Created by marko on 11.03.17..
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AuthUserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        final AuthUser user = userService.findByLoginName(username);
        if (user == null) {
            throw new BadCredentialsException(user + " - Niste baja za korištenje aplikacije.");
        }

        SimpleGrantedAuthority defaultRole = new SimpleGrantedAuthority("ROLE_USER");
        SimpleGrantedAuthority customRole = new SimpleGrantedAuthority("CUSTOM_ROLA");
        List<SimpleGrantedAuthority> authorities = Arrays.asList(defaultRole, customRole);
        UserPrincipal userDetails = new UserPrincipal(username, user.getLoginPassword(), authorities);
        userDetails.setAuthUser(user);

        return userDetails;
    }
}
package com.zadatak.service.impl;

import com.zadatak.domain.AuthUser;
import com.zadatak.repository.AuthUserRepository;
import com.zadatak.service.AuthUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by marko on 11.03.17.
 */
@Service
public class AuthUserServiceImpl implements AuthUserService {

    @Autowired
    private AuthUserRepository authUserRepository;

    @Override
    public AuthUser findByLoginName(String loginName) {
        return authUserRepository.findByLoginName(loginName);
    }

    @Override
    public AuthUser findOne(Long userId) {
        return authUserRepository.findOne(userId);
    }
}

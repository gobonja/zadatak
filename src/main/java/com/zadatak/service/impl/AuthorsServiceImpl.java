package com.zadatak.service.impl;

import com.zadatak.domain.Authors;
import com.zadatak.repository.AuthorsRepository;
import com.zadatak.service.AuthorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by marko on 12.03.17.
 */
@Service
public class AuthorsServiceImpl implements AuthorsService {

    @Autowired
    AuthorsRepository authorsRepository;

    @Override
    public List<Authors> findAll() {
        return authorsRepository.findAll();
    }

    @Override
    public Authors findOne(Long authorId) {
       return authorsRepository.findOne(authorId);
    }

    @Override
    public Authors save(Authors authors) {
        return authorsRepository.save(authors);
    }
}
package com.zadatak.service;

import com.zadatak.domain.Articles;

import java.util.List;

/**
 * Created by marko on 12.03.17.
 */
public interface ArticlesService {

    List<Articles> findAll();

    List<Articles> findByAuthUserLoginName(String loginName);

    Articles save(Articles articles);

    Articles findOne(Long articleId);

    void delete(Articles articles);

    void deleteById(Long id);

}

package com.zadatak.repository;

import com.zadatak.domain.Authors;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by marko on 12.03.17.
 */
public interface AuthorsRepository extends JpaRepository<Authors, Long> {
}

package com.zadatak.repository;

import com.zadatak.domain.Articles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by marko on 12.03.17.
 */
public interface ArticlesRepository extends JpaRepository<Articles, Long> {

    List<Articles> findByAuthUserLoginName(String loginName);

}

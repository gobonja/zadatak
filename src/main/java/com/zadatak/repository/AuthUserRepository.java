package com.zadatak.repository;

import com.zadatak.domain.AuthUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by marko on 11.03.17.
 */
public interface AuthUserRepository extends JpaRepository<AuthUser, Long> {

    AuthUser findByLoginName(String loginName);

}

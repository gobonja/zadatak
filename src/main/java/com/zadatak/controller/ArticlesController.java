package com.zadatak.controller;

import com.zadatak.domain.*;
import com.zadatak.security.SecurityUtil;
import com.zadatak.service.ArticlesService;
import com.zadatak.service.AuthUserService;
import com.zadatak.service.AuthorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by marko on 12.03.17.
 */
@Controller
@RequestMapping("/articles")
public class ArticlesController {

    private static final String MODEL_ATTRIBUTE_ARTICLES_FORM = "articlesForm";
    private static final String MODEL_ATTRIBUTE_AUTHORS = "authors";
    private static final String MODEL_ATTRIBUTE_ACTION = "action";

    @Autowired
    ArticlesService articlesService;

    @Autowired
    AuthorsService authorsService;

    @Autowired
    AuthUserService authUserService;

    @RequestMapping(value = {"/list"}, method = RequestMethod.GET)
    public String listArticles(@ModelAttribute("articlesSearchForm") ArticlesSearchForm articlesSearchForm, Model model) {
        final List<Articles> list;
        if (SecurityUtil.getAuthUserFromLoggedInUser().getRole().getRoleName().equals("admin")) {
            list = articlesService.findAll();
        } else {
            list = articlesService.findByAuthUserLoginName(SecurityUtil.getAuthUserFromLoggedInUser().getLoginName());
        }
        model.addAttribute("list", list);
        model.addAttribute("articlesSearchForm", articlesSearchForm);
        model.addAttribute("checkedItemsForm", new CheckedItemsForm());
        return "list";
    }

    @RequestMapping(value = {"/{loginName}"}, method = RequestMethod.GET)
    public String listArticlesByUser(@PathVariable("loginName") String loginName, Model model) {
        final List<Articles> list = articlesService.findByAuthUserLoginName(loginName);
        model.addAttribute("list", list);
        return "list";
    }

    @RequestMapping(value = {"/newarticle"}, method = RequestMethod.POST)
    public String addNewArticle(@Valid @ModelAttribute(MODEL_ATTRIBUTE_ARTICLES_FORM) ArticlesForm articlesForm, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {

        if (!bindingResult.hasErrors()) {
            Articles articles = new Articles();
            articles.setArticleHeadline(articlesForm.getArticleHeadline());
            articles.setArticleDate(articlesForm.getArticleDate());
            if ((articlesForm.getArticleUpvotes() != null)) {
                articles.setArticleUpvotes(articlesForm.getArticleUpvotes());
            } else {
                articles.setArticleUpvotes((long) 0);
            }

            articles.setArticleLink(articlesForm.getArticleLink());

            if (StringUtils.hasText(articlesForm.getVotedUsers().trim())) {
                articles.setVotedUsers(articlesForm.getVotedUsers());
            } else {
                articles.setVotedUsers(" ");
            }
            if (articlesForm.getUserId() != null) {
                articles.setAuthUser(authUserService.findOne(articlesForm.getUserId()));
            } else {
                articles.setAuthUser(SecurityUtil.getAuthUserFromLoggedInUser());
            }

            if (articlesForm.getAuthorId() == 0) {
                Authors authors = new Authors();
                authors.setAuthorName(articlesForm.getAuthorName());
                authorsService.save(authors);
                articlesForm.setAuthorId(authors.getAuthorId());
            }

            articles.setAuthors(authorsService.findOne(articlesForm.getAuthorId()));

            if (articlesForm.getArticleId() != null) {
                articles.setArticleId(articlesForm.getArticleId());
            }
            articlesService.save(articles);
            redirectAttributes.addFlashAttribute("articleSavedMessage", "Članak je spremljen");
            return "redirect:/articles/list";
        } else {
            List<Authors> authors = authorsService.findAll();
            model.addAttribute(MODEL_ATTRIBUTE_AUTHORS, authors);
        }
        model.addAttribute(MODEL_ATTRIBUTE_ARTICLES_FORM, articlesForm);
        return "newarticle";
    }

    @RequestMapping(value = {"/newarticle"}, method = RequestMethod.GET)
    public String renderNewArticle(@ModelAttribute(MODEL_ATTRIBUTE_ARTICLES_FORM) ArticlesForm articlesForm, Model model) {
        model.addAttribute(MODEL_ATTRIBUTE_ARTICLES_FORM, articlesForm);

        List<Authors> authors = authorsService.findAll();
        model.addAttribute(MODEL_ATTRIBUTE_AUTHORS, authors);
        return "newarticle";
    }

    @RequestMapping(value = {"/vote"}, method = RequestMethod.POST)
    @ResponseBody
    public ModelMap vote(VoteForm voteForm) {
        ModelMap map = new ModelMap();

        Articles articles = articlesService.findOne(voteForm.getArticleId());
        String loggedUser = SecurityUtil.getAuthUserFromLoggedInUser().getUserId().toString();
        if ("1".equals(voteForm.getType())) {
            if (articles.getVotedUsers().contains(loggedUser + "- ")) {
                articles.setArticleUpvotes(articles.getArticleUpvotes() + 2);
                articles.setVotedUsers(articles.getVotedUsers().replace(loggedUser + "- ", loggedUser + "+ "));
            } else {
                articles.setArticleUpvotes(articles.getArticleUpvotes() + 1);
                articles.setVotedUsers(articles.getVotedUsers() + loggedUser + "+ ");
            }

        } else if ("2".equals(voteForm.getType())) {
            if (articles.getVotedUsers().contains(loggedUser + "+ ")) {
                articles.setArticleUpvotes(articles.getArticleUpvotes() - 2);
                articles.setVotedUsers(articles.getVotedUsers().replace(loggedUser + "+ ", loggedUser + "- "));
            } else {
                articles.setArticleUpvotes(articles.getArticleUpvotes() - 1);
                articles.setVotedUsers(articles.getVotedUsers() + loggedUser + "- ");
            }
        }
        articlesService.save(articles);
        map.addAttribute("articleChanged", articles);
        return map;
    }

    @RequestMapping(value = {"/edit/{articleId}"}, method = RequestMethod.GET)
    public String editArticle(@PathVariable("articleId") Long articleId, Model model) {

        ArticlesForm articlesForm = new ArticlesForm();
        Articles articles = articlesService.findOne(articleId);

        articlesForm.setArticleHeadline(articles.getArticleHeadline());
        articlesForm.setArticleDate(articles.getArticleDate());
        articlesForm.setArticleUpvotes(articles.getArticleUpvotes());
        articlesForm.setVotedUsers(articles.getVotedUsers());
        articlesForm.setArticleLink(articles.getArticleLink());
        articlesForm.setUserId(articles.getAuthUser().getUserId());
        articlesForm.setAuthorId(articles.getAuthors().getAuthorId());
        articlesForm.setArticleId(articles.getArticleId());
        articlesForm.setLoginName(articles.getAuthUser().getLoginName());

        model.addAttribute(MODEL_ATTRIBUTE_ARTICLES_FORM, articlesForm);
        List<Authors> authors = authorsService.findAll();
        model.addAttribute(MODEL_ATTRIBUTE_AUTHORS, authors);
        model.addAttribute(MODEL_ATTRIBUTE_ACTION, "edit");

        return "newarticle";
    }

    @RequestMapping(value = {"/delete/{articleId}"}, method = RequestMethod.GET)
    public String deleteArticle(@PathVariable("articleId") Long articleId, Model model) {

        Articles articles = articlesService.findOne(articleId);
        articlesService.delete(articles);

        return "redirect:/articles/list";
    }

    @RequestMapping(value = "/delete-items", method = RequestMethod.POST)
    public String processForm(@ModelAttribute(value = "checkedItemsForm") CheckedItemsForm checkedItemsForm, RedirectAttributes redirectAttributes) {
        List<String> checkedItems = checkedItemsForm.getCheckedItems();
        if (checkedItems != null && !checkedItems.isEmpty()) {
            for (String idValue : checkedItems) {
                articlesService.deleteById(Long.valueOf(idValue));
            }
            redirectAttributes.addFlashAttribute("deleteMessage", "Obrisano " + checkedItems.size() + " članaka.");
        } else {
            redirectAttributes.addFlashAttribute("emptyDeleteMessage", "Ništa nije odabrano.");
        }
        return "redirect:/articles/list";
    }
}

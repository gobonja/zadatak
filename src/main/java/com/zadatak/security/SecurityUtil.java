package com.zadatak.security;

import com.zadatak.domain.AuthUser;
import com.zadatak.domain.UserPrincipal;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by marko on 13.03.17.
 */
public class SecurityUtil {

    private SecurityUtil() {
        // empty constructor
    }

    public static AuthUser getAuthUserFromLoggedInUser() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        return ((UserPrincipal) authentication.getPrincipal()).getAuthUser();
    }

}

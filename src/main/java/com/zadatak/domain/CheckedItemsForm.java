package com.zadatak.domain;

import java.util.List;

/**
 * Created by marko on 17.03.17.
 */
public class CheckedItemsForm {

    private List<String> checkedItems;

    public List<String> getCheckedItems() {
        return checkedItems;
    }

    public void setCheckedItems(List<String> checkedItems) {
        this.checkedItems = checkedItems;
    }
}

package com.zadatak.domain;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by marko on 12.03.17.
 */
public class ArticlesSearchForm {

    private Long articleId;

    private String articleHeadline;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date articleDate;

    private Long articleUpvotes;

    private String articleLink;

    private AuthUser authUser;

    private Authors authors;

    public Long getArticleUpvotes() {
        return articleUpvotes;
    }

    public void setArticleUpvotes(Long articleUpvotes) {
        this.articleUpvotes = articleUpvotes;
    }

    public String getArticleLink() {
        return articleLink;
    }

    public void setArticleLink(String articleLink) {
        this.articleLink = articleLink;
    }

    public AuthUser getAuthUser() {
        return authUser;
    }

    public void setAuthUser(AuthUser authUser) {
        this.authUser = authUser;
    }

    public Authors getAuthors() {
        return authors;
    }

    public void setAuthors(Authors authors) {
        this.authors = authors;
    }

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public String getArticleHeadline() {
        return articleHeadline;
    }

    public void setArticleHeadline(String articleHeadline) {
        this.articleHeadline = articleHeadline;
    }

    public Date getArticleDate() {
        return articleDate;
    }

    public void setArticleDate(Date articleDate) {
        this.articleDate = articleDate;
    }
}

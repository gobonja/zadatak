package com.zadatak.domain;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by marko on 11.03.17..
 */
public class UserPrincipal extends org.springframework.security.core.userdetails.User {

    public UserPrincipal(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    private AuthUser authUser;

    public AuthUser getAuthUser() {
        return authUser;
    }

    public void setAuthUser(AuthUser authUser) {
        this.authUser = authUser;
    }
}

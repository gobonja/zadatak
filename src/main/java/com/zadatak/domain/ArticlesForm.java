package com.zadatak.domain;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by marko on 12.03.17.
 */
public class ArticlesForm {

    private Long articleId;

    @NotBlank
    private String articleHeadline;

    @NotNull
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date articleDate;

    private Long articleUpvotes;

    @NotBlank
    private String articleLink;

    private String votedUsers;

    private Long userId;

    private String loginName;

    @NotNull
    private Long authorId;

    private String authorName;

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public String getArticleHeadline() {
        return articleHeadline;
    }

    public void setArticleHeadline(String articleHeadline) {
        this.articleHeadline = articleHeadline;
    }

    public Date getArticleDate() {
        return articleDate;
    }

    public void setArticleDate(Date articleDate) {
        this.articleDate = articleDate;
    }

    public Long getArticleUpvotes() {
        return articleUpvotes;
    }

    public void setArticleUpvotes(Long articleUpvotes) {
        this.articleUpvotes = articleUpvotes;
    }

    public String getArticleLink() {
        return articleLink;
    }

    public void setArticleLink(String articleLink) {
        this.articleLink = articleLink;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getVotedUsers() {
        return votedUsers;
    }

    public void setVotedUsers(String votedUsers) {
        this.votedUsers = votedUsers;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}

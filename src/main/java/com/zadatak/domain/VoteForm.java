package com.zadatak.domain;

/**
 * Created by marko on 16.03.17.
 */
public class VoteForm {

    private Long articleId;

    private String type;

    public void setId(final Long p_id) {
        articleId = p_id;
    }

    public String getType() {
        return type;
    }

    public void setType(final String p_type) {
        type = p_type;
    }

    public Long getArticleId() {
        return articleId;
    }

}

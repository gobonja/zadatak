package com.zadatak.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by marko on 11.03.17.
 */

@Entity
@Table(name = "AUTHORS")
public class Authors implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "AUTHOR_ID")
    private Long authorId;

    @Column(name = "AUTHOR_NAME", nullable = false)
    private String authorName;

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}

package com.zadatak.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by marko on 11.03.17.
 */

@Entity
@Table(name = "ARTICLES")
public class Articles implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ARTICLE_ID")
    private Long articleId;

    @Column(name = "ARTICLE_HEADLINE", nullable = false)
    private String articleHeadline;

    @Column(name = "ARTICLE_DATE", nullable = false)
    private Date articleDate;

    @Column(name = "ARTICLE_UPVOTES", nullable = false)
    private Long articleUpvotes;

    @Column(name = "ARTICLE_LINK", nullable = false)
    private String articleLink;

    @Column(name = "VOTED_USERS", nullable = true)
    private String votedUsers;

    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private AuthUser authUser;

    @ManyToOne
    @JoinColumn(name = "AUTHOR_ID", nullable = false)
    private Authors authors;

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public String getArticleHeadline() {
        return articleHeadline;
    }

    public void setArticleHeadline(String articleHeadline) {
        this.articleHeadline = articleHeadline;
    }

    public Date getArticleDate() {
        return articleDate;
    }

    public void setArticleDate(Date articleDate) {
        this.articleDate = articleDate;
    }

    public Long getArticleUpvotes() {
        return articleUpvotes;
    }

    public void setArticleUpvotes(Long articleUpvotes) {
        this.articleUpvotes = articleUpvotes;
    }

    public String getArticleLink() {
        return articleLink;
    }

    public void setArticleLink(String articleLink) {
        this.articleLink = articleLink;
    }

    public String getVotedUsers() {
        return votedUsers;
    }

    public void setVotedUsers(String votedUsers) {
        this.votedUsers = votedUsers;
    }

    public AuthUser getAuthUser() {
        return authUser;
    }

    public void setAuthUser(AuthUser authUser) {
        this.authUser = authUser;
    }

    public Authors getAuthors() {
        return authors;
    }

    public void setAuthors(Authors authors) {
        this.authors = authors;
    }
}
